"use strict";

module.exports = [{
  key: 'projects',
  query: 'le_prod/_table/project?related=interaction_by_project_id'
}, {
  key: 'interactions',
  query: 'le_prod/_table/interaction?related=contact_by_interaction_contact_join,employee_by_interaction_employee_join,location_by_interaction_location_join,outcome_by_interaction_outcome_join,project_by_project_id'
}, {
  key: 'employees',
  query: 'le_prod/_table/employee?related=interaction_by_interaction_employee_join'
}, {
  key: 'locations',
  query: 'le_prod/_table/location?related=interaction_by_interaction_location_join'
}, {
  key: 'contacts',
  query: 'le_prod/_table/contact?related=interaction_by_interaction_contact_join'
}];
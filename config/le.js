"use strict";

var _filters = require("../filters");

module.exports = {
  queries: [{
    key: 'projects',
    query: 'le_prod/_table/project?related=interaction_by_project_id'
  }, {
    key: 'interactions',
    query: 'le_prod/_table/interaction?related=contact_by_interaction_contact_join,employee_by_interaction_employee_join,location_by_interaction_location_join,outcome_by_interaction_outcome_join,project_by_project_id'
  }, {
    key: 'employees',
    query: 'le_prod/_table/employee?related=interaction_by_interaction_employee_join'
  }, {
    key: 'locations',
    query: 'le_prod/_table/location?related=interaction_by_interaction_location_join'
  }, {
    key: 'contacts',
    query: 'le_prod/_table/contact?related=interaction_by_interaction_contact_join'
  }],
  tabs: [{
    name: 'outreach',
    visualizations: [{
      id: 'interactionsByProjectPie',
      filter: _filters.countFilter,
      dataSubset: 'interactions',
      args: {
        countBy: 'project_name',
        subtree: true,
        subtreeName: 'project_by_project_id',
        subarray: false
      }
    }, {
      id: 'interactionsByRankPie',
      filter: _filters.countFilter,
      dataSubset: 'interactions',
      args: {
        countBy: 'rank',
        subtree: true,
        subtreeName: 'contact_by_interaction_contact_join',
        subarray: true
      }
    }, {
      id: 'interactionsByCollegePie',
      filter: _filters.countFilter,
      dataSubset: 'interactions',
      args: {
        countBy: 'college',
        subtree: true,
        subtreeName: 'contact_by_interaction_contact_join',
        subarray: true
      }
    }, {
      id: 'interactionsByDeptPie',
      filter: _filters.countFilter,
      dataSubset: 'interactions',
      args: {
        countBy: 'dept',
        subtree: true,
        subtreeName: 'contact_by_interaction_contact_join',
        subarray: true
      }
    }, {
      id: 'interactionsByStratPriorityPie',
      filter: _filters.countFilter,
      dataSubset: 'interactions',
      args: {
        countBy: 'strategic_priorities',
        subtree: false,
        subtreeName: '',
        subarray: false
      }
    }, {
      id: 'interactionsByLocationPie',
      filter: _filters.countFilter,
      dataSubset: 'interactions',
      args: {
        countBy: 'name',
        subtree: true,
        subtreeName: 'location_by_interaction_location_join',
        subarray: true
      }
    }]
  }]
};
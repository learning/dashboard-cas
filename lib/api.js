"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getData = getData;

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_axios.default.defaults.baseURL = process.env.URL_BASE;
_axios.default.defaults.headers.common['Accept'] = 'application/json';
_axios.default.defaults.headers.common['X-DreamFactory-Api-Key'] = process.env.DASHBOARD_KEY;

async function getData(calls) {
  let data = {};
  await calls.map((value, index) => {
    data[value.key] = [];
  });
  await makeCalls(data, calls);
  return data;
}

async function makeCalls(structure, calls) {
  let callsArray = calls.map(value => _axios.default.get(value.query));
  await _axios.default.all(callsArray).then(function (results) {
    calls.map((value, index) => {
      structure[value.key] = results[index].data.resource;
    });
  }).catch(error => console.log(error));
}
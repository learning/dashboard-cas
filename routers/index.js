"use strict";

require("core-js/modules/web.dom.iterable");

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _apiLearning = require("./api-learning");

Object.keys(_apiLearning).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _apiLearning[key];
    }
  });
});

var _apiLe = require("./api-le");

Object.keys(_apiLe).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _apiLe[key];
    }
  });
});

var _apiTale = require("./api-tale");

Object.keys(_apiTale).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _apiTale[key];
    }
  });
});
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Le = void 0;

var _express = require("express");

var _le = _interopRequireDefault(require("../config/le"));

var _lib = require("../lib");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = (0, _express.Router)();
router.get('/', async (req, res) => {
  res.send((await (0, _lib.getData)(_le.default.queries).then(async response => await callFilters(response))));
});

async function callFilters(data) {
  let formattedData = {};
  await _le.default.tabs.map(async (value, index) => {
    formattedData[value.name] = await value.visualizations.reduce((acc, current) => {
      acc[current.id] = current.filter(data[current.dataSubset], current.args);
      return acc;
    }, {});
  });
  return formattedData;
}

const Le = router;
exports.Le = Le;
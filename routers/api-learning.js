"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Learning = void 0;

var _express = require("express");

// import axios from 'axios'
const router = (0, _express.Router)();
router.get('/overview', async (req, res) => {
  res.send('Overview goes here.');
});
const Learning = router;
exports.Learning = Learning;
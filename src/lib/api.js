import axios from 'axios'

axios.defaults.baseURL = process.env.URL_BASE
axios.defaults.headers.common['Accept'] = 'application/json'
axios.defaults.headers.common['X-DreamFactory-Api-Key'] = process.env.DASHBOARD_KEY

export async function getData (calls) {
  let data = {}
  await calls.map((value, index) => {
    data[value.key] = []
  })
  await makeCalls(data, calls)
  return data
}

async function makeCalls (structure, calls) {
  let callsArray = calls.map(value => axios.get(value.query))

  await axios.all(callsArray)
    .then(function (results) {
      calls.map((value, index) => {
        structure[value.key] = results[index].data.resource
      })
    }).catch(error => console.log(error))
}

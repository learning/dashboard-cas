export function countFilter (data, args) {
  let newData = {}
  data.map((value, index) => {
    if (args.subarray && args.subtree) {
      value[args.subtreeName].map((value2, index2) => {
        subfilter(value2, index2, args.countBy, newData)
      })
    } else if (args.subtree) {
      subfilter(value[args.subtreeName], index, args.countBy, newData)
    } else {
      subfilter(value, index, args.countBy, newData)
    }
  })
  return Object.keys(newData).map(key => {
    return { id: key, label: key, value: newData[key] }
  })
}

function subfilter (value, index, countBy, newData) {
  if (value[countBy] && value[countBy] !== '' && value[countBy] in newData) {
    newData[value[countBy]] = newData[value[countBy]] + 1
  } else if (value[countBy] && value[countBy] !== '') {
    newData[value[countBy]] = 1
  }
}

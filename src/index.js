var express = require('express')
var cas = require('connect-cas')
var url = require('url')
var bodyParser = require('body-parser')
const path = require('path')
const cookieParser = require('cookie-parser')
const cookieSession = require('cookie-session')
const cookieKey = process.env.COOKIE_SECRET
import {
  Learning,
  Le,
  Tale
} from './routers'

cas.configure({ 'host': 'login.vt.edu/profile' })
var app = express()

app.use(bodyParser.json())
app.use(cookieParser(cookieKey))
app.use(cookieSession({ secret: cookieKey }))
app.enable('trust proxy')
app.use('/advanced', cas.serviceValidate(), cas.authenticate(), express.static(path.join(__dirname, 'advanced')))
app.use('/api/learning', Learning)
app.use('/api/le', Le)
app.use('/api/tale', Tale)

app.get('/login', cas.serviceValidate(), cas.authenticate(), function (req, res) {
  return res.redirect('/')
})

app.get('/logout', function (req, res) {
  if (!req.session) {
    return res.redirect('/')
  }
  // Forget our own login session
  if (req.session.destroy) {
    req.session.destroy()
  } else {
    // Cookie-based sessions have no destroy()
    req.session = null
  }
  // Send the user to the official campus-wide logout URL
  var options = cas.configure()
  options.pathname = options.paths.logout
  return res.redirect(url.format(options))
})

app.use(express.static(path.join(__dirname, 'main')))
app.use('*', express.static(path.join(__dirname, 'main/index.html')))

app.listen(3000, () => console.log('Server running on port 3000'))

import { Router } from 'express'
import config from '../config/le'
import { getData } from '../lib'

const router = Router()

router.get('/', async (req, res) => {
  res.send(await getData(config.queries).then(async (response) => await callFilters(response)))
})

async function callFilters (data) {
  let formattedData = {}
  await config.tabs.map(async (value, index) => {
    formattedData[value.name] = await value.visualizations.reduce((acc, current) => {
      acc[current.id] = current.filter(data[current.dataSubset], current.args)
      return acc
    }, {})
  })
  return formattedData
}

export const Le = router
